﻿using UnityEngine;
using System.Collections;

namespace IGD246.Week02 {
	public class SkeletonController : MonoBehaviour {
		public Animator SkeletonAnimator ;
		public int Hp;
		void Start() {
			Hp = 100;
		}

		// Update is called once per frame
		void Update() {
			if (Input.GetKey (KeyCode.A)) {
				SkeletonAnimator.SetFloat ("Speed", 1);
			} else { SkeletonAnimator.SetFloat ("Speed", 0);

			}
			if (Input.GetKey (KeyCode.D)) {
				SkeletonAnimator.SetTrigger ("Jump");
			} 
			if (Input.GetKey(KeyCode.LeftShift)) {
				SkeletonAnimator.SetBool ("Sprint", true);
			} 
			else {SkeletonAnimator.SetBool ("Sprint", false);
			}
			if (Input.GetKey (KeyCode.Space)) {
				SkeletonAnimator.SetInteger ("Hp", 0);
			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				SkeletonAnimator.SetInteger ("Hp", 100);
			}
		}
	}

}