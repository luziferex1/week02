﻿using UnityEngine;
using System.Collections;

public class AmandaController : MonoBehaviour {
		public Animator AmandaAnimator ;
		public int Hp;
		void Start() {
			Hp = 100;
		}

		// Update is called once per frame
		void Update() {
			if (Input.GetKey (KeyCode.A)) {
				AmandaAnimator.SetFloat ("Speed", 1);
			} else { AmandaAnimator.SetFloat ("Speed", 0);
			}
			if (Input.GetKey (KeyCode.W)) {
				AmandaAnimator.SetTrigger ("Jump");
			} 
			if (Input.GetKey(KeyCode.LeftShift)) {
				AmandaAnimator.SetBool ("Sprint", true);
			} 
			else {AmandaAnimator.SetBool ("Sprint", false);
			}
			if (Input.GetKey (KeyCode.LeftControl)) {
				AmandaAnimator.SetTrigger ("Roll");
			}
		}
	}

